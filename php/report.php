<?php error_reporting(0); ?>

<html>
<head>
  <link rel="stylesheet" href="assets/bootstrap.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="assets/main.css" media="screen" title="no title" charset="utf-8">
</head>
<body>

<div class="container-fluid">
  <div class="row">

    <div class="jumbotron">
        <h1>Rio Olympics Result Analysis</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent egestas dignissim lacus, at consequat purus porta in. Morbi vitae nisi et massa facilisis dictum. Pellentesque faucibus pellentesque lorem et fermentum. Vestibulum sapien mi, porttitor at scelerisque nec, viverra ac elit. Duis tincidunt at lacus in consequat.</p>
      </div>

        <table class="table">

          <thead>
            <tr>
              <th>#</th>
              <th>Country</th>
              <th>Gold</th>
              <th>Silver</th>
              <th>Bronze</th>
              <th>All Medals</th>
            </tr>
          </thead>
          <!-- This is the table header -->

          <tbody>
        	<?php
            $count=1;
            foreach($countries as $country)
            {
              $out="<tr>";
              $out.="<td>";
              $out.=$count;
              $out.="</td>";
              $out.="<td>";
              $out.=$country['name'];
              $out.="</td>";
              $out.="<td>";
              $out.=$country['goldCount'];
              $out.="</td>";
              $out.="<td>";
              $out.=$country['silverCount'];
              $out.="</td>";
              $out.="<td>";
              $out.=$country['bronzeCount'];
              $out.="</td>";
              $out.="<td>";
              $out.=$country['totalCount'];
              $out.="</td>";
              $count++;
              echo $out;
            }
            ?>
          </tbody>

        </table>

  </div>
</div>

</body>
</html>
