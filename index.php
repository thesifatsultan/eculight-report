<?php

		require 'php/connection.php';
		$ID = 48;
		//sql statement
		//get map data
		$query = "SELECT `post_content` FROM `wp_posts` WHERE `ID`= ".$ID."; ";
		//get the result and store it in {result}
		$result = $conn->query($query);

		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				//decode post_content into json object and save it in {map_json}
				$map_json = json_decode($row['post_content']);
				// var_dump($map_json);
				//get levels array and save it in {levels}
				$levels = $map_json->levels;
				}
			}
?>

<html>
<head>
	<title>ECU Lighting Report</title>
  	<link rel="stylesheet" href="style/bootstrap.css" media="screen" title="no title" charset="utf-8">
  	<link rel="stylesheet" href="style/main.css" media="screen" title="no title" charset="utf-8">
	<link rel="shortcut icon" type="image/png" href="img/favicon.jpg"/>
</head>
<body>

<div class="container-fluid">
  <div class="row">

    <div class="jumbotron">
        <h1>ECU Facility Management Report</h1>
        <p>The current status of all the light in the ECU campus</p>
      </div>

        <table class="table">

          <thead>
            <tr>
              <th>Campus</th>
              <th>Light ID</th>
              <th>Status</th>
              <th>Date Reported</th>
            </tr>
          </thead>
          <!-- This is the table header -->

          <tbody>
        	<?php
					foreach ($levels as $level) {

						$campus = $level->id;
						$locations = $level->locations;

						foreach($locations as $location)
						{

							$out="<tr>";
							$out.="<td>";
							$out.=$campus;
							$out.="</td>";
							$out.="<td>";
							$out.=$location->id;
							$out.="</td>";
							$out.="<td>";
							if (isset($location->lightstatus)) {
								$out.=$location->lightstatus;
							}
							$out.="</td>";
							$out.="<td>";
							$out.="</td>";
							echo $out;


						}

					}

            ?>
          </tbody>

        </table>

  </div>
</div>

</body>
</html>
